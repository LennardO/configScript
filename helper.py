import os
import subprocess

import re
import shutil
import tempfile
from tempfile import mkstemp

import getpass

from exceptions import *

from pathlib import Path


def running_as_root():
    return os.getuid() == 0


def run_command(command):
    cp = subprocess.run(command + " 2>&1", shell=True, capture_output=True, text=True)
    if cp.returncode != 0:
        raise ProcessReturnedErrorCode(cp.returncode, cp.stdout)
    return cp.stdout


def apt_install(packages):
    run_command("sudo apt install -y " + " ".join(packages))


def sed(pattern, substitute, filename):
    run_command(f"sudo sed -i -e 's/{pattern}/{substitute}/g' {filename}")
    return

    regex = re.compile(pattern)
    with tempfile.NamedTemporaryFile("w", delete=False) as tmp_file:
        with open(filename) as src_file:
            for line in src_file:
                tmp_file.write(regex.sub(substitute, line))

    shutil.copystat(filename, tmp_file.name)
    shutil.move(tmp_file.name, filename)


def write_root_file(lines, filename, permissions=0o644):
    if isinstance(lines,  str):
        content = [lines]
    with tempfile.NamedTemporaryFile("w", delete=False) as tmp_file:
        for line in lines:
            tmp_file.write(line)

    if os.path.isfile(filename):
        shutil.copystat(filename, tmp_file.name)
    else:
        os.chmod(tmp_file.name, permissions)

    run_command(f"sudo chown root:root {tmp_file.name}")
    run_command(f"sudo mv {tmp_file.name} {filename}")


def replace_lines(old_line_content, substitute, filename):
    sed("^" + old_line_content + "$", substitute, filename)


def get_username():
    return getpass.getuser()


def read_key_value_file(filename):
    result = {}
    with open(filename, "r") as file:
        for line in file:
            if line[0] == "#":
                continue
            key_sep_value = line.partition("=")
            result[key_sep_value[0]] = key_sep_value[2]
    return result


def get_distro():
    distro = "Unknown"

    if os.path.isfile("/etc/lsb-release"):
        dic = read_key_value_file("/etc/lsb-release")
        for key in dic:
            dic[key] = dic[key][0:-1]  # remove \n character
        distro = dic["DISTRIB_ID"]

    if distro == "LinuxMint":
        return "Mint"

    return distro
# todo:
# # https://unix.stackexchange.com/a/6348
# if [-f /etc/lsb-release]; then
#     . /etc/lsb-release
#     export DISTRO_ID=$DISTRIB_ID
# elif type lsb_release > /dev/null 2>&1; then
#     export DISTRO_ID=$(lsb_release - si)
# elif [-f /etc/os-release]; then
#     . /etc/os-release
#     export DISTRO_ID=$NAME
# fi
# echo "detected Distro: ${DISTRO_ID}"


def get_chassis_type():
    result = run_command("sudo dmidecode --string chassis-type")
    if result == "":
      return "Unknown"
    if result[-1] == '\n':
        result = result[0:-1]
    return result


def get_desktop_environment():
    desktop_env = "Unknown"

    output = run_command("echo \"$XDG_DATA_DIRS\" | grep -Eo 'xfce|kde|gnome' || true")[0:-1]
    if output == "xfce":
        return "Xfce"
    
    if "XDG_SESSION_DESKTOP" in os.environ:
        desktop_env = os.environ["XDG_SESSION_DESKTOP"]

    if desktop_env == "cinnamon":
        return "Cinnamon"

    return desktop_env


def copy(source, destination):
    source = Path(os.path.expanduser(source)).resolve()
    destination = Path(os.path.expanduser(destination)).resolve()
    shutil.copy(source, destination)


def create_symlink(target, symlink_path):
    target = Path(os.path.expanduser(target)).resolve()
    symlink_path = Path(os.path.expanduser(symlink_path)).resolve()

    if os.path.isdir(symlink_path):
        filename = os.path.basename(target)
        symlink_path = symlink_path / filename
    symlink_dir_path = os.path.dirname(symlink_path)

    if os.path.isfile(symlink_path):
        run_command(f"sudo rm {symlink_path}")

    if os.access(symlink_dir_path, os.W_OK):
        run_command(f"ln -s {target} {symlink_path}")
    else:
        run_command(f"sudo ln -s {target} {symlink_path}")


def remove_directory(dir_path):
    dir_path = Path(os.path.expanduser(dir_path)).resolve()

    if not os.path.exists(dir_path):
        return

    if not os.path.isdir(dir_path):
        print(f"warning: {dir_path} is not a directory; skipping removal")
        return

    if len(os.listdir(dir_path)) != 0:
        print(f"warning: directory {dir_path} is not empty; skipping removal")
        return

    shutil.rmtree(dir_path)


def add_line(newline, filepath):
    filepath = Path(os.path.expanduser(filepath)).resolve()

    if os.path.isdir(filepath):
        raise Exception(f"{filepath} is a directory")

    if os.path.isfile(filepath):
        with open(filepath, "r") as file:
            for line in file:
                if line[-1] == "\n":
                    line = line[0:-1]
                if line == newline:
                    return

    with open(filepath, "a") as file:
        file.write(newline + "\n")
