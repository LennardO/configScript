#!/usr/bin/env bash
set -e

#todo: swapgröße automatisch bestimmen

free -h
swapon --show

sudo swapoff /swapfile

sudo dd if=/dev/zero of=/swapfile bs=1G count=8

sudo mkswap /swapfile
sudo chmod 600 /swapfile
sudo swapon /swapfile

free -h
swapon --show

if ! grep -q "^/swapfile" /etc/fstab
then
	echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
fi
