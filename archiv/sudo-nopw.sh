#!/usr/bin/env bash
set -e

envsubst < files/sudoers.tmpl | sudo tee /etc/sudoers.d/$USER
sudo chmod 0440 /etc/sudoers.d/$USER
