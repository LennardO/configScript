

sudo apt-get install apt-transport-https ca-certificates gnupg
echo "deb https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install -y google-cloud-sdk


# manual steps: 
# gcloud init
# gcloud services enable compute.googleapis.com
# gcloud config set compute/region europe-west3
# gcloud config set compute/zone europe-west3-c
# gcloud auth application-default login
