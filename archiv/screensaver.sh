#!/usr/bin/env bash
set -e

if [ "${CHASSIS_TYPE}" = "Desktop" ] && [ "${DISTRO_ID}" = "LinuxMint" ]; then
	gsettings set org.cinnamon.desktop.screensaver lock-enabled false
	gsettings set org.cinnamon.desktop.session idle-delay 1800
	gsettings set org.cinnamon.settings-daemon.plugins.power lock-on-suspend false
	gsettings set org.cinnamon.settings-daemon.plugins.power sleep-display-ac 2700
elif [ "${DISTRO_ID}" = "LinuxMint" ]; then
	echo "disable PowerManagement->Brightness->\"On battery, dim screen when inactive\"" >> manualSteps
else
	echo "increase idle timer for screensaver, etc." >> manualSteps
fi
#todo: mx specific settings
#todo: bei laptop: disable PowerManagement->Brightness->"On battery, dim screen when inactive"
#todo: disable screensaver
