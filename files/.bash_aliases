# Git
alias gits='git status'
alias git-undo-add='git reset'
alias git-undo-commit='git reset --soft HEAD~'
#alias gitahead='git log origin/$(git rev-parse --abbrev-ref HEAD)..HEAD'
#alias gitaheaddiff='git diff origin/$(git rev-parse --abbrev-ref HEAD)..HEAD'
#alias gitdeleteallwithoutremote='git fetch -p && for branch in `git for-each-ref --format '%(refname) %(upstream:track)' refs/heads | awk '$2 == "[gone]" {sub("refs/heads/", "", $1); print $1}'`; do git branch -D $branch; done'    todo: syntax error
#alias gitsubtreeadd='git subtree add --squash --prefix'
#todo: maybe move some commands into scripts
if command -v podman &> /dev/null
then
    alias docker=podman
fi
