import helper

def make_action(func):
    def create_action(*args, **kwargs):
        def run_action():
            return func(*args, **kwargs)
        return run_action
    return create_action


@make_action
def apt_install(*packages):
    helper.run_command("sudo apt install -y " + " ".join(packages))


@make_action
def apt_purge(*packages):
    helper.run_command("sudo apt purge -y " + " ".join(packages))


@make_action
def run_command(command):
    helper.run_command(command)


@make_action
def flatpak_install(remote, app_id):
    helper.run_command(f"flatpak install -y {remote} {app_id}")


manualSteps = []
@make_action
def manual_step(description):
    global manualSteps
    manualSteps.append(description)


@make_action
def replace_lines(old_line_content, substitute, filename):
    helper.replace_lines(old_line_content, substitute, filename)


@make_action
def write_root_file(lines, filename):
    helper.write_root_file(lines, filename)


@make_action
def copy_file(source, destination):
    helper.copy(source, destination)


@make_action
def create_symlink(target, symlink_location):
    helper.create_symlink(target, symlink_location)


@make_action
def remove_directory(dir_path):
    helper.remove_directory(dir_path)


@make_action
def run_script(filename):
    helper.run_command("./scripts/" + filename)


@make_action
def add_line(line, filepath):
    helper.add_line(line, filepath)
