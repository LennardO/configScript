#!/usr/bin/env python3

import os
import helper
from actions import *
from Task import Task


@make_action
def prompt_seafile_sync():
    if not os.path.isdir(os.path.expanduser("~/Seafile/Confidential")):
        print("^"*30)
        print("login to Seafile - choose home as folder - sync libraries")
        print("...at least the library \"Confidential\" is needed")
        input("Press Enter to continue...")
        print("V"*30)



isWSL=False
desktopEnvironmentName = helper.get_desktop_environment()
chassisType = helper.get_chassis_type()
distroName = helper.get_distro()



aptUpgrade = Task("apt update & upgrade", [
        run_command("sudo apt update"), run_command("sudo apt upgrade -y")
    ])

podmanSteps = [ ]
if distroName == "Mint":
    podmanSteps = [ run_command("echo \"deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/ /\" | sudo tee /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"),
            run_command("curl -L \"https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.04/Release.key\" | sudo apt-key add -"),
            run_command("sudo apt update"),
            run_command("sudo apt upgrade -y")
        ]
podmanSteps = podmanSteps + [ apt_install("ca-certificates", "podman"), run_command("echo \"unqualified-search-registries = [\"docker.io\"]\"") ]
if isWSL:
	podmanSteps.append(run_script("podmanWSL.sh"))
podman = Task("Podman", podmanSteps)

docker = Task("Docker", [
        run_script("docker.sh"),
        manual_step("Log out and log back in so that your group membership is re-evaluated. (docker)")
    ])

slack = Task("Slack", [
        flatpak_install("flathub", "com.slack.Slack"),
        manual_step("login to Slack")
    ])
    
sudoWithoutPw = Task("Allow sudo without a password", write_root_file(f"{helper.get_username()} ALL=(ALL) NOPASSWD: ALL",
                                                          f"/etc/sudoers.d/{helper.get_username()}"))
                                                          
flatpak = Task("FlatPak", [
        apt_install("flatpak"),
        run_command("sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo")
    ])
    
basicTerminalTools = Task("Basic Terminal Tools", apt_install("htop", "curl", "software-properties-common", "apt-transport-https",
                                         "ca-certificates", "apt-utils", "wget", "gnupg", "jq", "tree", "xclip", "nano", "dos2unix"))
                                         
bashFiles = Task("Bash Files", [
        copy_file("files/.bashrc", "~"),
        copy_file("files/.profile", "~"),
        copy_file("files/.bash_aliases", "~")
    ])
    

kitty = Task("Kitty", [
        run_command("curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin launch=n"),
        run_command("mkdir -p ~/.local/bin"),
        # todo: install to location outside of home
        #create_symlink("~/.local/kitty.app/bin/kitty", "~/.local/bin/"),
        create_symlink("~/.local/kitty.app/bin/kitty", "/usr/bin/"),
        # run_command("sudo ln -s ~/.local/kitty.app/bin/kitty /usr/bin"),
        run_command("cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/"),
        run_command("sed -i \"s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g\" ~/.local/share/applications/kitty.desktop"),
        manual_step("set Kitty as the default terminal emulator"),
        #manual_step("add application shortcut of ~/.local/bin/kitty to Ctrl+Alt+T"),
        manual_step("if Cinnamon: purge gnome-terminal")
     ])

maintenanceGuiTools = Task("Maintenance gui tools", [apt_install("gdmap", "gparted", "gnome-disk-utility")])

vlc = Task("VLC", apt_install("vlc"))

geany = Task("Geany", apt_install("geany"))

pdfToPngTerminalTool = Task("Convert PDF to PNG with pdftoppm in.pdf > out.png", apt_install("poppler-utils"))

okular = Task("Okular", apt_install("okular"))

pdfarranger = Task("pdfarranger", apt_install("pdfarranger"))

caffeine = Task("caffeine", apt_install("caffeine"))

flameshot = Task("flameshot", apt_install("flameshot"))

markText = Task("MarkText", [
        flatpak_install("flathub", "com.github.marktext.marktext"),
        manual_step("adjust tab with in MarkText to 2: File->Preferences->Markdown->Preferred tab width"),
        manual_step("...and enable File->Auto Save")
    ])
    
drawio = Task("draw.io", flatpak_install("flathub", "com.jgraph.drawio"))

libreOffice = Task("LibreOffice", [
        apt_install("libreoffice-writer", "libreoffice-impress", "libreoffice-calc", "libreoffice-math"),
        manual_step("LibreOffice Addon installieren: https://extensions.libreoffice.org/en/extensions/show/texmaths-1")
    ])
    
thunderBird = Task("Thunderbird", [
        apt_install("thunderbird", "hunspell-de-de", "birdtray"),
        manual_step('''login to thunderbird
        ...File/Edit->Preferences->Composition->HTML Style: disable "Use Paragraph format instead of Body Text by default"
        ...View->Message Body As->Simple HTML  (suggested for security reasons (efail?))
        '''),
        manual_step('''start birdtray and add account
        ...and enable all hiding/start features (except the last one for resetting the icon?)
        ...and enable using a "different icon when unread" (under Settings->General); use files/white32x32.png
        add Birdtray to startup applications
        ''')
    ])
    
webBrowser = Task("Web Browser", [
        apt_install("firefox", "chromium"),
        manual_step("login to firefox"),
        manual_step("import CAD expressions")
    ])

turtle = Task("Turtl", [
        run_script("turtl.sh"),
        manual_step("login to Turtl")
    ])

thunar = Task("Thunar", [
        apt_install("thunar"),
        manual_step("open thunar: View->Location Selector->Toolbar Style"),
        manual_step("set Edit->Preferences->Behavior->Navigation to \"Double click to activate items\""),
        manual_step("add shortcuts of important directories to Thunar"),
        apt_install("zenity"),
        run_command("mkdir -p ~/.config/Thunar"),
        copy_file("files/createNewFile.sh", "~/.config/Thunar/"),
        # todo: use preferred application instead of hard coded kitty
        # e.g. for MX: <command>exo-open --working-directory %f --launch TerminalEmulator</command>
        copy_file("files/thunar-actions.xml", "~/.config/Thunar/uca.xml"),
        copy_file("files/thunar-shortcuts.scm", "~/.config/Thunar/accels.scm"),
        manual_step("set Thunar as the default file explorer"),
        manual_step("if Cinnamon: purge nemo")
    ])
    
seafile = Task("Seafile", [
        run_script("seafile.sh"),
        prompt_seafile_sync()
    ])
    
removeDefaultDirectories = Task("Remove Default Directories", [
        remove_directory("~/Documents"),
        remove_directory("~/Pictures"),
        remove_directory("~/Videos"),
        remove_directory("~/Music"),
        remove_directory("~/Public")
    ])

spotify = Task("Spotify", [
        flatpak_install("flathub", "com.spotify.Client"),
        manual_step("login to Spotify\n...disable announcements\n...add to startup applications")
    ])

fwupd = Task("fwupd (aka fwupdmgr)", [
        apt_install("fwupd")
   ])

xfceSettings = Task("Xfce Settings", [
        copy_file("files/xfce4-keyboard-shortcuts.xml", "~/.config/xfce4/xfconf/xfce-perchannel-xml"),
        manual_step("Power Manager -> Display: increase timer"),
        apt_purge("xfce4-screenshooter", "qpdfview", "clementine")
    ])

cinnamonSettings = Task("Cinnamon Settings", [
        manual_step("enable \"Only use workspaces on primary monitor.\" (Settings->Workspaces)"),
        manual_step("enable \"disable special key to move and resize windows\" (Settings->Windows->Behavior)"),
        manual_step("add \"workspace switcher\" and \"force quit\" applets"),
        manual_step("disable system sounds"),
        apt_purge("celluloid", "xed")
    ])

# umgesetzte Lösung: https://unix.stackexchange.com/a/277488
# alternative Lösung?: https://github.com/milaq/XMousePasteBlock
disableMiddleMouseButtonPaste = Task("Disable Middle Mouse Button Paste", [
            apt_install("xbindkeys", "xsel", "xdotool"),
            add_line("\"echo -n | xsel -n -i; pkill xbindkeys; xdotool click 2; xbindkeys\"", "~/.xbindkeysrc"),
            add_line("b:2 + Release", "~/.xbindkeysrc"),
            run_command("xbindkeys -p"),
            copy_file("files/xbindkeys.desktop", "~/.config/autostart/")
        ])
        
signal = Task("Signal", flatpak_install("flathub", "org.signal.Signal"))

telegram = Task("Telegram", [
        flatpak_install("flathub", "org.telegram.desktop"),
        manual_step("login to Telegram\n...and enable send with ctrl+enter, disable large emojis, enable startup")
    ])

aptCleanup = Task("Cleanup", run_command("sudo apt autoclean -y && sudo apt clean -y && sudo apt autoremove -y"))

basicDevTerminalTools = Task("Basic Dev Terminal Tools", apt_install("gdb", "doxygen", "python3-pip"))

teamspeak = Task("Teamspeak", [
        flatpak_install("flathub", "com.teamspeak.TeamSpeak"),
        # damit pulse audio nicht alles andere muted sobald eine anwendung mit dem phone-tag was abspielt:
        replace_lines("load-module module-role-cork", "#load-module module-role-cork", "/etc/pulse/default.pa"),
        manual_step("login to Teamspeak")
    ])

# todo: depends on nvm
ungit = Task("ungit", [
        run_command("sudo -H npm install -g ungit")
    ])

copyGitConfig = Task("Copy Git Config", [
        copy_file("files/.gitconfig", "~"),
        copy_file("files/.gitconfig-personal", "~")
    ])

copySshKeys = Task("Copy SSH Keys", [
        run_script("ssh.sh")
    ])

jetbrainsToolbox = Task("Jetbrains", [
        run_script("jetbrains-toolbox.sh"),
        manual_step('''
login to jetbrains-toolbox
...do not create a password for the Default Keyring
...install CLion
...increase memory usage to 8192  #todo: what memory usage and where is this option?
...install PyCharm and IntelliJ
...enable IDE Settings SYNC with silent plugin sync enable''')
    ])

gitlabrunner = Task("GitlabRunner", run_script("gitlab-runner.sh"))

openjdk17 = Task("OpenJDK 17", [ 
        apt_install("openjdk-17-jdk"),
        add_line("export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64", "~/.profile"),
        add_line("export PATH=\"$JAVA_HOME/bin:$PATH\"", "~/.profile")
    ])

maven3_8 = Task("Maven 3.8", [
        run_script("maven3_8.sh"),
        add_line("export M2_HOME=/usr/local/apache-maven/apache-maven-3.8.5", "~/.profile"),
        add_line("export M2=$M2_HOME/bin", "~/.profile"),
        add_line("export MAVEN_OPTS=\"-Xms256m -Xmx512m\"", "~/.profile"),
        add_line("export PATH=$M2:$PATH", "~/.profile")
    ])

#todo: depends on .bashrc, etc.
nvm = Task("nvm", [
      run_command("curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash"),
      run_command(". ~/.bashrc"),
      run_command("nvm install node")
  ])

keycloakCli = Task("Keycloak CLI", [
      run_command("wget https://github.com/keycloak/keycloak/releases/download/17.0.1/keycloak-17.0.1.zip"),
      run_command("unzip keycloak-17.0.1.zip"),
      run_command("rm keycloak-17.0.1.zip"),
      run_command("mkdir -p ~/bin"),
      run_command("mv keycloak-17.0.1 ~/bin"),
      create_symlink("~/bin/keycloak-17.0.1/bin/kc.sh", "~/bin/kc"),
      create_symlink("~/bin/keycloak-17.0.1/bin/kcadm.sh", "~/bin/kcadm"),
      create_symlink("~/bin/keycloak-17.0.1/bin/kcreg.sh", "~/bin/kcreg"),
  ])

freecad = Task("FreeCAD", run_script("freecad.sh"))

intellij = Task("IntelliJ IDEA Community", flatpak_install("flathub", "com.jetbrains.IntelliJ-IDEA-Community") )

go = Task("GO", [
      run_command("wget https://go.dev/dl/go1.18.linux-amd64.tar.gz"),
      run_command("sudo tar -C /usr/local -xzf go*.linux-amd64.tar.gz"),
      run_command("sudo sh -c 'echo \"export PATH=$PATH:/usr/local/go/bin\" >> /etc/profile'")
  ])

kind = Task("kind", [
      run_command("go install sigs.k8s.io/kind@v0.12.0"),
      run_command("echo \"export PATH=$(go env GOPATH)/bin:$PATH\" >> ~/.profile")
  ])






basicsTerminal = [ aptUpgrade, sudoWithoutPw, flatpak, basicTerminalTools, bashFiles ]

basicsGui = [ kitty, geany, maintenanceGuiTools, vlc, markText, drawio, thunar, webBrowser, removeDefaultDirectories, flameshot, caffeine ]

pdfTools = [ pdfToPngTerminalTool, pdfarranger, okular ]

office = [ libreOffice ]

dev = [ podman, basicDevTerminalTools ]

personalFiles = [ seafile ]

ide = [ intellij ]

java17 = [ openjdk17, maven3_8 ]

git = [ copyGitConfig, copySshKeys ]

music = [ spotify ]

workCommunication = [ slack ]

personalCommunication = [ signal, telegram, teamspeak ]

personalNotes = [ turtle ]

cleanup = [ aptCleanup ]

xfce = [ xfceSettings ]

cinnamon = [ cinnamonSettings ]

archiv = [ ungit, jetbrainsToolbox, gitlabrunner, nvm, freecad, fwupd, go, kind ]



desktopEnvironmentSpecific = [ ]
if desktopEnvironmentName == "Xfce":
    desktopEnvironmentSpecific = xfce
if desktopEnvironmentName == "Cinnamon":
    desktopEnvironmentSpecific = cinnamon

chassisSpecific = [ ]
if chassisType == "Notebook":
    # todo: and if ThinkPad bzw. nur bei laptops mit trackpoint und physischen maustasten:
    chassisSpecific = [ disableMiddleMouseButtonPaste ]




commonConfig = basicsTerminal + basicsGui + pdfTools + office + dev + desktopEnvironmentSpecific + chassisSpecific

extendedCommonConfig = commonConfig + personalFiles + java17 + git + music + ide

workConfig = extendedCommonConfig + workCommunication

commonPersonalConfig = extendedCommonConfig + personalCommunication + personalNotes

mainConfig = commonPersonalConfig

laptopConfig = commonPersonalConfig



selectedConfig = workConfig



selectedConfig = selectedConfig + cleanup


print("starting configScript")

if helper.running_as_root():
    raise RuntimeError("the configScript is running as root")

print("detected Distro:", helper.get_distro())
print("detected Desktop Environment:", helper.get_desktop_environment())
print("detected Chassis Type:", helper.get_chassis_type())

print(f"\nRunning {len(selectedConfig)} Tasks:")
print("-"*30)
for task in selectedConfig:
    print(task.name)
    task.execute()
print("-"*30)

print("\nManual Steps:")
print("-"*30)
for step in manualSteps:
    print(step)
print("-"*30)
