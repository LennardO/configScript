#!/usr/bin/env bash
set -e

mkdir -p ~/.config/containers
echo "[engine]
events_logger = \"file\"" > ~/.config/containers/containers.conf
